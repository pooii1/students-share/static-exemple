package main;

import java.time.LocalDate;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class StudentTest {
	
	/*
	 * Il est correct d'avoir quelques Assertions pour un test.
	 */
	@Test
	void whenCreatingStudentThenObjectInitializedCorrectly() {
		int expectedMatricule = MatriculeGenerator.peekMatricule();
		Student cat = new Student("Catwoman", "catwoman@dc.com");
		
		Assertions.assertEquals(expectedMatricule, cat.getMatricule());
		Assertions.assertEquals("Catwoman", cat.getName());
		Assertions.assertEquals("catwoman@dc.com", cat.getEmail());
		Assertions.assertEquals(LocalDate.now().getYear(), cat.getYearOfAdmission());
	}

	/*
	 * Voici comment tester une valeur statique.  Il ne faut pas avoir une valeur hard-coded, sinon les tests sont dépendants les uns des autres.
	 * Il faut savoir quelle valeur est attendue pour ce test.
	 */
	@Test
	void whenCreatingStudentThenMatriculeIsGeneratedAutomatically() {
		int expectedMatricule = MatriculeGenerator.peekMatricule();
		Student cat = new Student("Cat", "cat@cat.com");

		Assertions.assertEquals(expectedMatricule, cat.getMatricule());
	}

	/*
	 * Test avec plusieurs étudiants et leur matricule.
	 */
	@Test
	void whenCreatingMultipleStudentsThenMatriculesAreGeneratedAutomatically() {
		int expectedMatricule1 = MatriculeGenerator.peekMatricule();
		Student cat1 = new Student("Cat", "cat@cat.com");
		int expectedMatricule2 = MatriculeGenerator.peekMatricule();
		Student cat2 = new Student("Cat", "cat@cat.com");

		Assertions.assertEquals(expectedMatricule1, cat1.getMatricule());
		Assertions.assertEquals(expectedMatricule2, cat2.getMatricule());
	}

	@Test
	void whenModifyingYearOfAdmissionWithFutureYearThenModificationIsAccepted() {
		Student cat = new Student("Cat", "cat@cat.com");
		int validYearOfAdmission = LocalDate.now().getYear() + 1;

		cat.setYearOfAdmission(validYearOfAdmission);

		Assertions.assertEquals(validYearOfAdmission, cat.getYearOfAdmission());

	}

	/*
	 * Exemple de test pour une exception.
	 */
	@Test
	void whenModifyingYearOfAdmissionWithPartYearThenModificationIsRefused() {
		Student cat = new Student("Cat", "cat@cat.com");
		int invalidYearOfAdmission = LocalDate.now().getYear() - 1;

		Assertions.assertThrows(PastYearException.class, () -> cat.setYearOfAdmission(invalidYearOfAdmission));
	}

}
