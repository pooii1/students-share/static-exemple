package main;

/*
 * Classe pour générer automatiquement les identifiants.
 * 
 * Cette 'fonctionnalité' est dans une classe séparée pour respecter le principe de la responsabilité unique (SRP).
 */
public class MatriculeGenerator {
	
	public static final int MAX_MATRICULE = 1000000;
	private static int matriculeSequence = 1;
	
	public static int peekMatricule() {
		return matriculeSequence;
	}
	
	public static int getNextMatricule() {
		int nextMatricule = matriculeSequence;
		matriculeSequence++;
		
		return nextMatricule;
	}
}
