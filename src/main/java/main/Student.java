package main;

import java.time.LocalDate;

public class Student {

	private int matricule;
	private String name;
	private String email;
	private int yearOfAdmission;

	/*
	 * Exemple de surchage de constructeur.  On utilise le this() pour appeler un constructeur à l'intérieur de la classe elle-même.
	 */
	public Student() {
		this(MatriculeGenerator.getNextMatricule(), "Student", "student1@student.com");
	}

	public Student(String name, String email) {
		this(MatriculeGenerator.getNextMatricule(), name, email);
	}

	public Student(int matricule, String name, String email) {
		this.matricule = matricule;
		this.name = name;
		this.email = email;
		this.yearOfAdmission = LocalDate.now().getYear();
	}

	public int getMatricule() {
		return matricule;
	}

	public String getName() {
		return name;
	}

	public String getEmail() {
		return email;
	}

	public int getYearOfAdmission() {
		return yearOfAdmission;
	}

	/*
	 * Exemple pour le lancement d'une exception
	 */
	public void setYearOfAdmission(int year) {
		if (LocalDate.now().getYear() <= year) {
			this.yearOfAdmission = year;
		} else {
			throw new PastYearException("Year is in the past.");
		}
	}

	/*
	 * Exemple de surchage de méthodes.  Même nom, mais un nombre ou des types de paramètres différents.
	 */
	public void setAll(int matricule, String name, String email) {
		this.matricule = matricule;
		this.name = name;
		this.email = email;
		this.yearOfAdmission = LocalDate.now().getYear();
	}

	public void setAll(String name, String email) {
		this.setAll(MatriculeGenerator.getNextMatricule(), name, email);
	}
}
