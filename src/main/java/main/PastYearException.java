package main;

/*
 * Cette exception est un exemple d'exception personnalisée.
 * 
 * Les points importants sont:
 * 	- Elle hérite d'IllegalArgumentException, ce qui en fait une RuntimeException
 *  - Le numéro de série est facultatif et n'est pas utilisé en POO2.
 *  - Le constructeur prend un message d'erreur et utilise le constructeur de son parent (super).
 */
public class PastYearException extends IllegalArgumentException {

	private static final long serialVersionUID = 1L;
	
	public PastYearException(String message) {
		super(message);
	}
}
